#include <iostream>
using namespace std;
#include "stdlib.h"
#include "stdio.h"
#include "time.h"
  static char  buf[64];
  char * dtToA( const struct tm  * dtTm, char *b =0) {
   if (dtTm) {
     if (b==0)
       b=buf;
     sprintf(b, "%04d/%02d/%02d"
               , 1900+dtTm->tm_year        // ��� ���������� � 1900
                      , 1+dtTm->tm_mon     // ������ ����� ����� 0
                          , dtTm->tm_mday  // ���� ���������� � 1
     );
     return b;
   }
   return 0;                                                                       
  }
  class  x {
     struct tm  * dtTm;
    public:
      x () { 
         time_t dt = time(0);
         dtTm = localtime(&dt);
       }
    friend  ostream& operator<< (ostream& os,  const x *v);
    friend  ostream& operator<< (ostream& os,  const x& v);
  };
  ostream& operator<< (ostream& os, const x * v) {
     if (v) {
       os << dtToA(v->dtTm);
     }
     return os;
  }
  ostream& operator<< (ostream& os, const x& v) {
       return os<<dtToA(v.dtTm);
  }
 
  int main() {
     x t;
     cout<< (&t); // usage of pointer   ���������
     cout << ' ';
     cout <<t;  // usage of reference ������
     cout << " :one";

     cerr<< "\n"<<&t<< " "<< t << " :two";
     operator<<(operator<<(cerr.put('\n'),  &t
                          ).put(' '
                                )  , t
     )<<" :three";                      
     return 0;
  }                     