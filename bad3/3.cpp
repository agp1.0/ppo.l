#include <iostream>
using namespace std;
 
void change(const int& pt) {
    int *pc;
    pc = const_cast<int&>(pt);	//<-- �������� ������
    *pc*=-1;						//<-- �������� ������
}
 
int main() {
    int pop1 = 38383;
    const int pop2 = 2000;		 //<-- �������� ������
    cout << "pop1/pop2: " << pop1 << '/' << pop2 << '\n';
    change(pop1);
    change(pop2);			//<-- �������� ������
//    change(3);			//<-- �������� ������
    cout << "pop1/pop2: " << pop1 << '/' << pop2 << '\n';
}