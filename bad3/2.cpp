// compile with: /EHsc
#include <iostream>

using namespace std;
struct CCTest {
     void set( int );
     void print() const;
     int number;
};

void CCTest::set( int num ) { number = num; }

void CCTest::print() const {
   cout << "\nBefore: " << number;
   const_cast< CCTest * >( this )->number--;
   cout << "\nAfter: " << number;
}
int main() {
   CCTest X;
   X.set( 8 );
   X.print(); // number is changed
}