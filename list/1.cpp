#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <crtdbg.h>

void checkMemory() {
#ifdef _DEBUG

//  ��������� ������� �������� �� �������� ������
     _CrtSetDbgFlag(
                 _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)
              | _CRTDBG_CHECK_ALWAYS_DF 
              | _CRTDBG_LEAK_CHECK_DF
        );

//  ��� ���� ���������
//  �������� � ���� ������������ ������

     _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
     _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDERR);
     _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);
#endif
}

char * strRplc(char * oldstr, const char * str) {
   char * rc = 0;
   if (oldstr){        // ���������� ������ ������
   	  free(oldstr);
   	  oldstr = 0;
   }
   if (str) {          // �������� ����� 
      size_t l = strlen(str);
      rc  = (char *) malloc (l+1);
      strcpy(rc, str);
   }
//     fprintf (stderr,           "\nstrRplc %d/'%s'", rc, rc );

   return rc;
}

struct  line { 
       char *        text; // ������, � ���� ������ ������ �� ����� 
       line *        next; // ��������� ������� ������.
                           // ���� next ==0, �� ����.�������� ���
};

line *  add( line * list, const char * data) {
  line * rc = list;
  line *  elem = (line *) malloc (sizeof (line));
  if (elem){          //  ���� ������ ���������, �� ���������
     elem->next = 0;
     elem->text = strRplc(0, data);
     elem->next = list;   // � ������ �������� �������� ���� ����o�
     rc = (line *)elem;
  }                   // ����� �� ���� ������ list
  return rc;
}
line *  del( line * list) {
  line * rc = 0;
  if (list){          //  ���� ������ ����, ��
     rc = list->next; //  ������� �������
     if (list->text) {free(list->text); list->text=0;}
     free (list); list = 0;
  }                   // ����� �� �� ���� �������
  return rc;
}

void  print (const line * list) {
  line * tmp = (line *)list;
  for (tmp = (line *)list;  
         tmp;                // ���� �� ����� �� �����, �� �������
          tmp = tmp->next){     // ������� � ���������� �������� ������
    if (tmp->text)              //  ������ � �������� ���� �������
       printf ("\n'%s'", tmp->text);
    else                        //  ������ � �������� ���
       printf ("\n'%s'", "<null>");
  }
}

line * mkList(const char * firstData) {
  line * rc = (line *)malloc(sizeof(line));
  if (rc) {
      rc->next=0;
      rc->text =  strRplc(0, firstData);
  }
  return rc;
}

line * delList(line * list){
  while( list){
    list = del(list);
  }
  return list;
}

int main() {
  checkMemory();
  malloc(13);
  line * lst = mkList("hi");
  lst = add(lst, "bye");
  print(lst);
  lst = delList(lst);
}

