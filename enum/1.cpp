#include <stdio.h>
#include <stdlib.h>
enum class Suit { Diamonds, Hearts, Clubs, Spades };
int main( int argc, char * argv[]) {
  Suit x;
  if (argc > 1) {
    x = (Suit)atoi(argv[1]);
    switch (x) {
      case Suit::Clubs    : printf ("Clubs"); break;
      case Suit::Diamonds : printf ("Diamonds"); break;
      case Suit::Hearts   : printf ("Hearts"); break;
      case Suit::Spades   : printf ("Spades"); break;
      default:              printf ("wrong");
    }
  }
  else 
    printf ("nothing to do!");
  return (int)x;
}