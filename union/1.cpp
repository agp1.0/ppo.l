
#include <stdio.h>
 union  intOrder {
   int   i;
   char  bytes[sizeof(int)];
 };
 int main() {
    intOrder val;
    val.i = 0;
    val.bytes[0] = 1;
    printf("\n0th byte set: %u ", val.i);
    val.i = 0;
    val.bytes[1] = 1;
    printf("\n1st byte set: %u ", val.i);
    val.i = 0;
    val.bytes[2] = 1;
    printf("\n2nd byte set: %u ", val.i);
    val.i = 0;
    val.bytes[3] = 1;
    printf("\n3th byte set: %u ", val.i);
 }

