#include <iostream>
  
   template <typename T>
     void swap1(T& l, T& r) {
      T t = l;
      l = r;
      r = t;
   }
   int main() {
      int a = 1, b = 0;
      std::cout<<a<<b;
      swap1(a, b);    // swap1(int&  , int& )  is generated here
      std::cout<<"\n"<<a<<b;
      double p = 3.14, q = 2.71;
      swap1(p,q);    // swap1(double&  , double& )  is generated here
      return 0;
   }

     