#include <stdio.h>
#include <malloc.h>


class  coor {
   public:
     double x;          //  ��� �� �����
     double y;          //  ��� ����� ������
     coor  ();
     ~coor ();
     static void ini (coor * this1, double x, double y);
};
   void coor::ini (coor * this1, double x, double y ) {
     if (this1) {
        this1->x = x;
        this1->y = y;
     }
   }
   coor::coor() {
     printf ( "\ncoor () is here");
     ini(this, 0.0, 0.0);
   }
   coor::~coor() {
     printf ( "\ndestructor coor () is here");
   }

#include "memwatch.h"
#define new mwNew
#define delete mwDelete


int main() {
   malloc(2);                //                       line 30 ������ ������ ������
   coor * foo = new coor();  //  coor () is here                
   delete foo;               //  destructor coor () is here     
   foo = new coor();         //    coor () is here    line 33 ������ ������ ������           
   coor * foo1 = new coor[1];//  coor () is here                      
   delete[] foo1;            //  destructor coor () is here           
   foo1 = new coor[1];       //     coor () is here   line 36 ������        
   new int(15);              //                            37 ���������
   new int[1];               //                            38 �����
}