#include "stdlib.h"
#include "stdio.h"
#include "time.h"
  static char  buf[64];
  char * dtToA ( const struct tm  * dtTm, char *b =0) {
   if (dtTm){
     if (b==0)
       b=buf;
     sprintf(b, "%04d/%02d/%02d"
               , 1900+dtTm->tm_year        // ��� ���������� � 1900
                      , 1+dtTm->tm_mon     // ������ ����� ����� 0
                          , dtTm->tm_mday  // ���� ���������� � 1
     );
     return b;
   }
   return 0;                                                                       
  }

//  struct  x {     // ��������� ���������� ��� ��������
  class  x {
     struct tm  * dtTm;
    public:
      x () { 
         time_t dt = time(0);
         dtTm = localtime(&dt);
       }
     //  ������������� �������� ���������� ����
     operator char* () const  {   return dtToA(dtTm);     }
  };

  int main() {
     x t;
     char *y = t;   // ������� ���������� ���� 
     printf ("%s",  y);                                           
     return 0;
  }                     