#include "stdlib.h"
#include "stdio.h"
#include "lib.h"  // ����� ������������� �������
#define  SEPARATOR " \t"
  class  coor {
   public:
    double x;          //  ��� �� �����  ��������� ������� ������ :))
    double y;          //  ��� ����� ������
    coor();
    coor(double x, double y);
    coor( const char * str);
    ~coor ();
    static void ini (coor * this1, double x, double y);
  };
  void coor::ini (coor * this1, double x, double y ) {
     if (this1) {
        this1->x = x;
        this1->y = y;
     }
  }
  coor::coor() {
    printf ( "\ncoor () is here");
    ini(this, 0.0, 0.0);
  }
  coor::coor(double x, double y) {
    printf ( "\ncoor (double x, double y) is here");
    ini(this, x, y);
  }
  coor::coor( const char * str1) {
    double x=0.0, y=0.0;
    printf ( "\ncoor ( const char * str1) is here");
    if (str1 && strlen(str1)> 0){
      char * str = strRplc(0, str1);         // don't change str1 
      char * xs  = strtok  (str, SEPARATOR); // to make copy
      if (xs)   {
        if (isDouble(xs)){
           x = atof (xs);
           char * ys  = strtok  (0, SEPARATOR);
           if (ys)
              if (isDouble(ys))
                 y = atof(ys);
        }
      }
      free(str); str=0;
    }
    ini(this, x, y);
  }
  coor::~coor() {
       printf ( "\ndestructor coor () is here");
  }

  int main() {
    coor *a = new coor();
    printf ("\n x/y: %.6f/%.6f", a->x, a->y); delete a; a=0;
    a = new coor(2.718281828, 3.14159);
    printf ("\n x/y: %.6f/%.6f", a->x, a->y); delete a; a=0;
    coor c("2.2 3.3");              // usage ctor
    printf ("\n x/y: %.6f/%.6f", c.x, c.y); 
  }                                 // usage c destructor 