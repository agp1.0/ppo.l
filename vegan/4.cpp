#include <iostream>
#include <typeinfo>
using namespace std;
 // this example has been taken from A Theory Of Objects. L.Cardelli, M. Abadi 
 struct   food  {};          //  ������� ����� - ���

 struct vegetables: food { 
     vegetables(food &v){printf ("\n>>no any meet more<<");}
     vegetables(){}
 }; // ����������� ����� - ������������ ��� - ������������ ���
 
 template <class T>
 struct P  {
    void eat (T f){
       printf ("\nI ('%s') have swallowed '%s'", typeid( this ).name(), typeid( f ).name());
    } 
 };

 template <class T>
 void dinner(P<T> p, food f ){
   printf ("\nI  am feeding  '%s' with '%s'", typeid(p ).name(), typeid( f ).name());
   p.eat( f); 
 }

 int main(){
   P<food> person;
   P<vegetables>  vegetarian;  // �������������� ������ ����������
   food meat;
   vegetables onion ;
//   dinner (person, 1);
//   dinner (person, meat);
//   dinner (person, onion);
//   dinner (vegetarian, onion);
   dinner (vegetarian, meat);
 }
