#include <iostream>
   struct   coor2  {      // base class
      double  lat;
      double  lon;
      coor2(){
         lat=1.1;
         lon=2.2;
      }
      void printE() const{
         printf("\n coor2 object: %.6f, %.6f", lat, lon);
      }
      virtual  void printL() const {
         printf("\n coor2 object: %.6f, %.6f", lat, lon);
      }
   };
   struct coor3: coor2 {  // derived class
      double alt;         // altitude
      coor3():coor2() {   // usage of ctor of base class
        alt = 3.3;
      }
      void printE() const {
         printf("\n coor3 object: %.6f, %.6f, %.2f", lat, lon, alt);
      }
      void printL() const override  {
         printf("\n coor3 object: %.6f, %.6f, %.2f", lat, lon, alt);
      }
   };
   void pri (coor2& o) {
     printf("\npri:");
     o.printE(); // here would be called the base class method
     o.printL(); // here would be called the right method
   }
   int main() { 
     coor3 b;    
     coor2 c;
     b.printE();  // coor3 method
     b.printL();  // coor3 method
     pri(b);
     pri(c);   
     return 0;
   }
