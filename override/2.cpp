#include <iostream>
   struct   coor2  {      // base class
      double  lat;
      double  lon;
      coor2(){
         lat=1.1;
         lon=2.2;
      }
      void printE()         const { printf("\n printE() ");        }
      void printE(int p)    const { printf("\n printE(int p)");    }
   };
   struct coor3: coor2 {        // derived class
      // to avoid of hiding printE names uncomment this line
      using coor2::printE;      // Now printE is visible!
      void printE(char p) const {  
        printf("\n printE(char p): I am going to call printE()");  
        coor2::printE();        // this is how to call any base methods
      } 
   };
   int main() {
     coor2 b ;    
     coor3 c ;    
     b.printE();        
     b.printE(2);      
     c.printE();  // no any error here because of using coor2::printE;
     c.printE(1); // it is possible to use any methods from coor2
   }
