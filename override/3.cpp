#include <iostream>
   struct   coor2  {      // base class
      double  lat;
      double  lon;
      coor2(){
         lat=1.1;
         lon=2.2;
      }
      void printE()         const { printf("\n printE() ");        }
      void printE(int p)    const { printf("\n printE(int p)");    }
      void printE(double p) const { printf("\n printE(double p)"); }
   };
   struct coorX: coor2 {      };
   struct coor3: coorX {        // derived class
      void printE(char p) const {  
        printf("\n printE(char p);");  
      } 
   };
   int main() {
     coorX b ;    
     coor3 c ;    
     b.printE(); // no  any names hiding      is here
     b.printE(3.14149); 
     b.printE(2);      
     c.printE(); // the compilation error is here because of hiding names 
     c.printE('1');  
   }
