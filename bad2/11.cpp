#include <stdlib.h>
#include <string.h>
#include <iostream>

   template <typename T>
   void eqv(T l) {
      std::cerr<<" 1";
   }
   struct wrd {
      char  * txt;
      wrd(const char *v) {
        txt = 0;
        if (v) {
          txt = (char *) malloc(strlen(v)+1);
          strcpy(txt, v);
        }
      }
      ~wrd() {
        if (txt) {
          free(txt);     // error is here
          txt =0;
        }
      }
/*      wrd (const wrd& v) { // copy constructor
        this->txt = 0;
        if (v.txt) {
          txt = (char *) malloc(strlen(v.txt)+1);
          strcpy(this->txt, v.txt);
        }
      } */ 
   };
   int main() {
     {
       wrd a("one");
       eqv(a);           // new copy is here
       std::cerr<<" 2";
     }                  // error is here
     std::cerr<<" 3";
     return 0;
   }

