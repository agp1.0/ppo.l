#include <stdlib.h>
#include <string.h>
#include <iostream>

   struct wrd {
      char  * txt;
      wrd(const char *v) {
        txt = 0;
        if (v) {
          txt = (char *) malloc(strlen(v)+1);
          strcpy(txt, v);
        }
      }
      ~wrd() {
        if (txt) {
          free(txt);     // error is here
          txt =0;
        }
      }
/*     wrd& operator= (const wrd& val) {  //assigment operator
       if (this != &val) {                 // to sure this are the different objects
        if (txt)  {
          free(txt); 
          txt = 0;
        }
        if (val.txt) {
          txt = (char *) malloc(strlen(val.txt)+1);
          strcpy(txt, val.txt);
        }
       }
       return *this;
      }  */
   };    

   int main() {
     {
       wrd a("one"), b("two");
       a = b;   // both of objects references on the same txt now
       std::cerr<<" 2";
     }
     std::cerr<<" 3";
     return 0;
   }