#include <stdlib.h>
#include <iostream>

   template <typename T>
//     void swap1(T l, T r) {
     void swap1(T& l, T& r) {
      T t = l;
      l = r;
      r = t;
      std::cerr<<" 1";
   }
   struct wrd {
      char  * txt;
      wrd(const char *v) {
        txt = 0;
        if (v) {
          txt = (char *) malloc(strlen(v)+1);
          strcpy(txt, v);
        }
      }
      ~wrd() {
        if (txt) {
          free(txt);     // error is here
          txt =0;
        }
      }
   };
   int main() {
     {
       wrd a("one"), b("two");
       swap1(a,b);
       std::cerr<<" 2";
     }
     std::cerr<<" 3";
     return 0;
   }

