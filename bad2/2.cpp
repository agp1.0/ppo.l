



MGZ1::MGZ1( const MGZ1&  obj ):MGZ( (MGZ&)obj)                  // копирования
{
 // Copy constructors
}




MGZ1 & MGZ1::operator = (const MGZ1 & val)       // присвоения
{
  if (this != &val) {
    if (_dbg)  {
      fprintf(stderr, "\n%s::-assign  to %x from %x", _me, this, &val);
      fprintf(stderr, "\n%s::-i'll forget %d-th object to remember %d-th object",  _me, oNo, val.oNo);
    }
    //((Film*)this)->operator=((const Film&)val);
    Film::operator=(val);
    descr=x_rplcStr(descr, val.descr);
    w=val.w;
    l=val.l;
  }
  return *this;
}

