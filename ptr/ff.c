#include <stdio.h>
double plus     (int a, int b) { return ((double)a + b);}
double minus    (int a, int b) { return ((double)a - b);}
double divide   (int a, int b) { return ((double)a / b);}
double multiply (int a, int b) { return ((double)a * b);}

void ff (int a, int b, const char * nm, double (*f) (int z, int y) = 0 ){
  if (nm && f)  
    printf ("%d %s %d = %.1f \n", a,  nm, b, f(a,b));
  else 
    printf ("nothing to do!\n");
}

int main() {
  double (*arr[])(int z, int y) = 
          {plus, minus, divide, multiply};
  const char * names [] = {
     "plus", "minus", "divide", "multiply"
  };
  int a = 3, b = 2;
  for (int i = 0; i < 4; i++) 
    ff (a, b, names[i], arr[i]);

  ff (1, 0, "divide", divide);
   1.0/0.0;
  perror ("divide by zero");
}
