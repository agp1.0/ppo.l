#include <stdio.h>

double minus    (int a, int b) { return (double)(a - b);}
double divide   (int a, int b) { return (double)(a / b);}

struct iif{
  int a;
  int b;
  double (*f) (int z, int y) = 0;
};


double fff ( const char * nm, iif * obj ){
  double ret;
  printf ("%d %s %d = %.1f \n", 
      obj->a,  nm, obj->b, (ret= obj->f(obj->a,obj->b)));
  return ret;
}


int main() {
  iif o = {6, 0, divide};
  fff ("divide", &o);
  o.f  = minus;
  fff ("minus", &o);
}
