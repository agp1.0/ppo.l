#include <stdio.h>

double minus    (int a, int b) { return (double)(a - b);}
double divide   (int a, int b) { return (double)(a / b);}

struct iif{
  int a;
  int b;
  iif() {
     a=6;
     b=2;
  }
   //virtual 
   double f (int z, int y) {return divide(a,b);};
};
struct iif2: iif{
//   double f (int z, int y) override  {return minus(a,b);};
   double f (int z, int y)   {return minus(a,b);};
};

double fff ( const char * nm, iif * obj ){
  double ret;
  printf ("%d %s %d = %.1f \n", 
      obj->a,  nm, obj->b, (ret= obj->f(obj->a,obj->b)));
  return ret;
}
int main() {
  iif   o ;
  iif2 o2 ;
  fff ("divide", &o);
  fff ("minus", &o2);
}
