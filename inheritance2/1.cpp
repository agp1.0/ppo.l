#include <iostream>
  struct   coor  {               // base class
     double  x=1.0;
  };
//   struct bad : coor, coor {}; // error is here
//   struct a: virtual coor{};
//   struct d: virtual coor{};
  struct a: coor{};
  struct d: coor{};
  struct coor3:  a,  d {         // derived class
     double alt = 2.0;           // altitude
  };
  int main() {
    coor3 b;
   // ��� ������� ���� x �� ������ ������� � ���� x  �� ������� �������
    printf("\n%.6f, %.6f, %.2f", b.a::x, b.d::x, b.alt);
    printf("\nsizes %d/%d", sizeof (coor), sizeof(coor3));
    return 0;
  }