#include <stdio.h>
#include <stdlib.h>
   class coor {
     public:
      double  lat;
      double  lon;
      char  * txt;
      ~coor() {
        fprintf(stderr,"\ndestructor 1");
        if (txt) {
          free(txt);     // error is here
          txt =0;
        }
        fprintf(stderr,"\ndestructor 2");
      }
   };
   int main() {
     coor b = {1.1, 2.2, "123"};  // usage the initializer list is error
     fprintf(stderr,"\ngoing to crash");
   }

