#include <stdio.h>
#include <stdlib.h>
//   struct coor {
   class coor {
     public:
      double  lat;
      double  lon;
      char  * txt;
      coor() {                       // default  constructor
        fprintf(stderr,"\nctor 1"); 
        lat = 0.0; lon = 0.0; txt = 0;
      } 
      coor(double x) {              
        fprintf(stderr,"\nctor 2"); 
        lat = x; lon = x;  txt = 0;
      } 
      ~coor() {
        fprintf(stderr,"\ndestructor started");
        if (txt) {
          free(txt); txt = 0;
        }
        fprintf(stderr,"\ndestructor finished");
      }
   };
   int main() {
     coor b ;                        // to use default constructor
     coor *c = new coor(3.1415926);
     coor *arr = new [3];            // to use default constructor for array
//     delete [] arr; arr = 0;
//     delete c     ; c = 0;
     fprintf(stderr,"\ngoing to stop");
   }                                 // destructor for b used here

