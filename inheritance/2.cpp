#include <iostream>
   struct   coor1  {
      double  lat;
      coor1() {
         lat=1.11;
      }
   };
   class    coor2: public coor1 { // access specifier is important here
   public:
      double  lon;
      coor2():coor1() {
         lon=2.22;
      }
   };
   struct coor3:  coor2 {
      double A;// altitude
      coor3():coor2()      {
        A = 3.33;
      }
   };
   int main() {
     coor3 b ;    
     printf("\n %.6f, %.6f, %.2f", b.lat, b.lon, b.A);
     return 0;
   }