#include <iostream>
   struct   coor2  {
      double  B;
      double  L;
      coor2() {
         B=1.1;
         L=2.2;
      }
   };
   struct coor3: coor2 {
      double A; // altitude
      coor3():coor2()      {
        A = 3.3;
      }
   };
   int main() {
     coor3 b ;    
     b.B *= 10.0;
     b.L *= 100.0;
     coor2 *p2 = &b; // no any convertation operator
     coor3 *p  = 0;
     p = (coor3*)p2; // it is right   coor3 -> coor2 -> coor3
     printf("\n %.6f, %.6f %.2f", p->B, p->L, p->A);
     coor2 p1;
     p2 = &p1;
     p = (coor3*)p2;  /// this is bad coor2-> coor2 -> coor3
     printf("\n %.6f, %.6f %.2f", p->B, p->L, p->A);
      
     return 0;
   }