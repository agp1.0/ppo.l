#include <iostream>
   struct   coor2  {      // base class
      double  lat;
      double  lon;
      coor2() {
         lat=1.1;
         lon=2.2;
      }
   };
   struct coor3: coor2 {  // derived class
      double alt;         // altitude
      coor3():coor2() {   // usage of ctor of base class
        alt = 3.3;
      }
   };
   int main() {
     coor3 b ;    
     printf("\n %.6f, %.6f, %.2f", b.lat, b.lon, b.alt);
     return 0;
   }