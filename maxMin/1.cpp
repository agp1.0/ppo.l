#include <iostream>
   // this examle has been taken from A Theory Of Objects. L.Cardelli, M. Abadi 
   struct   maxClass  {      // base class
      int n;
      maxClass (int v) {n = v;};
      virtual
      maxClass *  max (maxClass  * other) {
        maxClass * ret = 0;
        printf ("\nmaxClass::max is here") ;
        if (other) {
          if (this->n > other->n)
            ret = this;
          else 
            ret = other;
        }
        return ret;
      } 
   };
   struct minMaxClass: maxClass {  
      minMaxClass (int v): maxClass(v) {};
      minMaxClass *  min (minMaxClass  * other) {
        minMaxClass * ret = 0;
        if (other) {
          if (this->n < other->n)
            ret = this;
          else 
            ret = other;
        }
        return ret;
      } 
   };
   struct minMaxClass1: minMaxClass {  
      minMaxClass1 (int v): minMaxClass(v) {};
      minMaxClass1 *  max (maxClass  * other) override  {
        printf ("\nminMaxClass1::max is here: %x", other) ;
        minMaxClass1 * ret = 0;
        if (other) {

          minMaxClass1 *o = dynamic_cast<minMaxClass1 * >(other);
            printf ("\n2  %x", o) ;
//          minMaxClass1 *o = (minMaxClass1 * )other;
          if (o->min(this) == other) {   /// error is here

//          if (other->min(this) == other) {   /// error is here
            ret = this;
          }
          else {
            printf ("\n this.n/o.n/o/other:  %d/%d/%x/%x", this->n, o->n, o, other) ;
            printf ("\n4") ;
            ret = o;
          }
        }
        return ret;
      }   
   };

   int main() { 
     minMaxClass1 mm1 (0);
//     minMaxClass1 mm1 (10);
     maxClass     m   (3);
     try {
       if (mm1.max(&m)!=0)
         printf ("\nminMaxClass1 is subtype of maxClass!") ;
     }
     catch(...){  // any exception
         printf ("\nminMaxClass1 is NOT subtype of maxClass!") ;
     }
     return 0;
   }
