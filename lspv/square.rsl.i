\kw{scheme} Square =\\
\hspace*{2mm} \kw{class}\\
\hspace*{2mm} \hspace*{2mm} \kw{type} Figure, UReal = \LTYPEBRACE\space\hspace*{2mm} r : \kw{Real} \RDOT\space  r > 0\DOT\space 0 \RTYPEBRACE\space\hspace*{2mm}\\
\hspace*{2mm} \hspace*{2mm} 	\\
\hspace*{2mm} \hspace*{2mm} \kw{value}\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} SetWidth : Figure \TIMES\space  UReal \RIGHTARROW\space  Figure,\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} SetHeight : Figure \TIMES\space  UReal \RIGHTARROW\space  Figure,\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} GetWidth : Figure \RIGHTARROW\space  UReal,\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} GetHeight : Figure \RIGHTARROW\space  UReal\\
\hspace*{2mm} \hspace*{2mm} 	\\
\hspace*{2mm} \hspace*{2mm} \kw{axiom}\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \LBRACKET\space Sgw\_sw\RBRACKET\space\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \ALL \space w : UReal, f : Figure \RDOT\space \\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} GetWidth(SetWidth(f, w)) = w,\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \LBRACKET\space Sgw\_sh\RBRACKET\space\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \ALL \space h : UReal, f : Figure \RDOT\space \\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} GetWidth(SetHeight(f, h)) = h,\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \LBRACKET\space Sgh\_sh\RBRACKET\space\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \ALL \space h : UReal, f : Figure \RDOT\space \\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} GetHeight(SetHeight(f, h)) = h,\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \LBRACKET\space Sgh\_sw\RBRACKET\space\\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \ALL \space w : UReal, f : Figure \RDOT\space \\
\hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} \hspace*{2mm} GetHeight(SetWidth(f, w)) = w\\
\hspace*{2mm} \kw{end}\\
